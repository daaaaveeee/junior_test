<?php
    session_start();
    require 'db/DB.php';

    // Checks if save form has been submited
    if(isset($_POST['saveForm'])){

        $db = new App\DB();

        // Checks if submited form meets all requirements
        $errors = null;
        if(count($db->find('products', 'sku', $_POST['sku'])) > 0) $errors[] = "Product with this SKU already exists!";
        if(strlen($_POST['sku']) <= 0 || strlen($_POST['sku']) > 9) $errors[] = "SKU is required and can't be longer than 9 symbols.";
        if(strlen($_POST['name']) <= 0 || strlen($_POST['name']) > 128) $errors[] = "Name is required and can't be longer than 128 symbols.";
        if(strlen((string) $_POST['price']) <= 0 || strlen((string) $_POST['price']) > 10 || !is_numeric($_POST['price']) || $_POST['price'] < 0) $errors[] = "Price is required, can't be longer than 10 numbers and must contain only positive numbers.";

        // if type switcher has been set, then checks type requierments.
        if(isset($_POST['type'])){
            switch ($_POST['type']) {
                case 'size':
                    if(strlen($_POST['size']) <= 0 ) $errors[] = "Size is required";
                    break;

                case 'dimensions':
                    if(strlen($_POST['height']) <= 0 ) $errors[] = "Height is required";
                    if(strlen($_POST['width']) <= 0 ) $errors[] = "Width is required";
                    if(strlen($_POST['length']) <= 0 ) $errors[] = "Length is required";
                    break;

                case 'weight':
                    if(strlen($_POST['weight']) <= 0 ) $errors[] = "Weight is required";
                    break;

                default:
                    $errors[] = 'Type must be chosen.';
                    break;
            }
        } else {
            $errors[] = 'Type must be chosen.';
        }

        // If errors has not been set, then proceeds
        if(!$errors){

            // Creates string of specification according type
            switch ($_POST['type']) {
                case 'size':
                    $specs = $_POST['size'];
                    break;

                case 'dimensions':
                    $specs = $_POST['height'] . "x" . $_POST['width'] . "x" . $_POST['length'];
                    break;

                case 'weight':
                    $specs = $_POST['weight'];
                    break;

                default:
                    $specs = "Unknown";
                    break;
            }

            // inserts in database product
            $insert = $db->insert('products', [
                'sku' => "'" . htmlspecialchars($_POST['sku']) . "'", 
                'name' => "'" . htmlspecialchars($_POST['name']) . "'", 
                "price" => $_POST['price'], 
                "specs" => "'" . htmlspecialchars($specs) . "'"
            ]);

        }
    }
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Style -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="sass/style.css">

    <!-- Title -->
    <title>Product Add</title>
</head>
<body>

    <!-- Top bar -->
    <div id="top" class="container-fluid pb-2 mb-4 mt-3">
        <div class="row">
            <div class="col-sm-3">
                <h3>Product Add</h3>
            </div>
            <div class="col mt-1">
                <a href="index.php">Product List</a>
            </div>
            <div class="col-3 text-right">
                <button type="submit" form="saveFrom" class="btn btn-success btn-sm">Save</button>
            </div>
        </div>
    </div>

    <!-- Content with form and messages -->
    <div id="add" class="container-fluid">
        <div class="row">
            <!-- Messages -->
            <div class="col-12">
                <?php if(isset($insert) && $insert == true): ?>
                    <div class="alert alert-success" role="alert">
                        <h6>Product successfully added!</h6>
                    </div>
                <?php endif; ?>

                <?php if(isset($errors) && $errors != []): ?>
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            <?php foreach($errors as $error): ?>
                                <li><?= $error ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>

            <!-- Product form -->
            <div class="col-sm-4">

                <form id="saveFrom" method="POST" action="<?= $_SERVER['PHP_SELF']; ?>">

                    <input name="saveForm" type="hidden" value="1">

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">SKU</label>
                        <div class="col-sm-10">
                            <input type="text" name="sku" class="form-control" value="" >
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" value="" >
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Price</label>
                        <div class="col-sm-10 input-group">
                            <input type="text" name="price" class="form-control" value="" >
                            <div class="input-group-append">
                                <span class="input-group-text">$</span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Type Switcher</label>
                        <div class="col-sm-9">
                            <select id="selectType" name="type" class="custom-select" onchange="typeSwitcher(this)">
                                <option value="0" selected="selected" disabled>Type Switcher</option>
                                <option value="size">DVD-disc</option>
                                <option value="dimensions">Furniture</option>
                                <option value="weight">Book</option>
                            </select>
                        </div>
                    </div>

                    <div id="size">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Size</label>
                            <div class="col-sm-10">
                                <input type="text" name="size" class="form-control" value="" >
                            </div>
                        </div>
                        <p>Please provide product size. (Example: 100MB, 3GB...)</p>
                    </div>

                    <div id="dimensions">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Height</label>
                            <div class="col-sm-10">
                                <input type="text" name="height" class="form-control" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Width</label>
                            <div class="col-sm-10">
                                <input type="text" name="width" class="form-control" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Length</label>
                            <div class="col-sm-10">
                                <input type="text" name="length" class="form-control" value="">
                            </div>
                        </div>
                        <p>Please provide product dimensions, Height, Width, Length. (Example: 10cm, 220mm)</p>
                    </div>

                    <div id="weight">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Weight</label>
                            <div class="col-sm-10">
                                <input type="text" name="weight" class="form-control" value="">
                            </div>
                        </div>
                        <p>Please provide product weight. (Example: 100g, 2KG...)</p>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <!-- Function for type switcher -->
    <script>
        function typeSwitcher(value) {

            // Hides all type windows.
            $('#size').hide();
            $('#dimensions').hide();
            $('#weight').hide();

            // Show selected type window.
            switch($(value).val()) {
                case 'size':
                    $('#size').show();
                    break;

                case 'dimensions':
                    $('#dimensions').show();
                    break;

                case 'weight':
                    $('#weight').show();
                    break;

                default:
            }
        }
    </script>
</body>
</html>