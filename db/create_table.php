<?php

    // This creates table in database, if dump are not imported or database is empty.

    include __DIR__ . '/DB.php';

    $query = "CREATE TABLE products(
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        sku VARCHAR(9) NOT NULL UNIQUE,
        name VARCHAR(128) NOT NULL,
        price FLOAT(10,2) NOT NULL,
        specs VARCHAR(128) NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
    );";

    $result = (new App\DB)->query($query);
    if($result) echo "Database table created!\n";
?>