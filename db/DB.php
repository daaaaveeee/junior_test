<?php
namespace App;

use mysqli;

class DB{

    // Database connection credentials
    protected $data = [
        'host' => 'localhost',
        'username' => 'dave',
        'password' => 'password',
        'database' => 'products'
    ];

    private $connection;

    public function __construct(){

        // Creates connection with database.
        $this->connection = new mysqli($this->data['host'], $this->data['username'], $this->data['password'], $this->data['database']);

        if ($this->connection->connect_errno) {
            echo "Failed to connect to MySQL: " . $this->connection->connect_error;
            exit();
        }
    }

    // Gets data from database.
    public function select(String $table, Array $columns = []){

        $columns = ($columns == []) ? '*' : implode (", ", array_keys($columns));

        $result = $this->connection->query("SELECT $columns FROM $table;");
        if(!$result) return die($this->connection->error);

        $rows = $result->fetch_all(MYSQLI_ASSOC);
        return $rows; // Returns array of multiple rows
    }

    // Inserts new data into database.
    public function insert(String $table, Array $data){

        $columns = implode (", ", array_keys($data));
        $values = implode (", ", array_values($data));

        $result = $this->connection->query("INSERT INTO $table ($columns) VALUES ($values);");
        if(!$result) return die($this->connection->error);
        
        return $result; // Returns boolean
    }

    // Deletes choosed rows in databse
    public function delete(String $table, String $column, Array $values){

        $data = implode (", ", array_values($values));

        $result = $this->connection->query("DELETE from $table WHERE $column IN ($data);");
        if(!$result) return die($this->connection->error);
        
        return $result; // Returns boolean.
    }

    // Finds specific value by column in database
    public function find(String $table, String $column, String $value){

        $result = $this->connection->query("SELECT * FROM $table WHERE $column='$value';");
        if(!$result) return die($this->connection->error);

        $rows = $result->fetch_all(MYSQLI_ASSOC);
        return $rows; // Returns array of row
    }

    // Can be used if need to send plain query into database.
    public function query(String $query){
        $result = $this->connection->query($query);
        if(!$result) die($this->connection->error);
        
        return $result; // Returns result of query
    }

    public function __destruct(){
        // Connection close
        $this->connection->close();
    }

}