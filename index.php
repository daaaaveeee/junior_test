<?php
    session_start();
    require 'db/DB.php'; 

    $db = new App\DB();

    // Checks if form has been submited
    if(isset($_POST['productForm'])){        

        // Checks for choosed action
        switch ($_POST['action']) {
            case 'delete':

                // Checks it atleast one product is selected, if not sets error message.
                $errors = null;
                if(!$_POST['toDelete']) $errors[] = "Select atleast one product!";

                // If no errors has been set, then deletes selected products
                if(!$errors){
                    $delete = $db->delete('products', 'id', $_POST['toDelete']);
                }
                break;
            
            default:
                break;
        }
    }

    // Gets all products from database
    $products = $db->select('products'); 
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="sass/style.css">

    <!-- Title -->
    <title>Product New</title>
</head>
<body>

    <!-- Top bar -->
    <div id="top" class="container-fluid pb-2 mb-4 mt-3">
        <div class="row">
            <div class="col-sm-2">
                <h3>Product List</h3>
            </div>
            <div class="col mt-1">
                <a href="add.php">Product Add</a>
            </div>
            <div class="col-sm-3">
                <div class="form-row">
                    <div class="col">
                        <select name="action" form="productForm" class="form-control form-control-sm">
                            <option value="delete">Mass Delete Action</option>
                        </select>
                    </div>
                    <div class="col-auto">
                        <button type="submit" form="productForm" class="btn btn-primary btn-sm">Apply</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Product list -->
    <div id="list" class="container-fluid">

        <form id="productForm" method="POST">
            <input name="productForm" type="hidden" value="1">
            <div class="row justify-content-center">

                <!-- Messages -->
                <div class="col-12">
                    <?php if(isset($delete) && $delete == true): ?>
                        <div class="alert alert-success" role="alert">
                            <h6>Products successfully deleted!</h6>
                        </div>
                    <?php endif; ?>

                    <?php if(isset($errors) && $errors != []): ?>
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                <?php foreach($errors as $error): ?>
                                    <li><?= $error ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <?php if(count($products) < 1): ?>
                        <div class="alert alert-secondary" role="alert">
                            No products found!
                        </div>
                    <?php endif; ?>
                </div>

                <!-- Displaying products -->
                <?php foreach($products as $product): ?>

                    <div class="card mb-3 ml-3 mr-3">
                        <div class="card-body text-center pt-0 pb-0">
                            <dl>
                                <dd class="ml-1 mb-0 text-left">
                                    <input class="form-check-input position-static" type="checkbox" name="toDelete[]" value="<?=$product['id']?>">
                                </dd>
                                <dd><?= $product['sku'] ?></dd>
                                <dd><?= $product['name'] ?></dd>
                                <dd><?= $product['price'] ?>$</dd>
                                <dd>
                                    <?php
                                        if(preg_match("/^(\d+(?:,\d+)?(mm|MM|cm|CM|m|M))x(\d+(?:,\d+)?(mm|MM|cm|CM|m|M))(?:x(\d+(?:,\d+)?(mm|MM|cm|CM|m|M)))$/", $product['specs'])){
                                            echo "Dimensions: " . $product['specs'];
                                        } else if(preg_match("/^(\d+(?:,\d+)?)\s*(g|G|kg|KG)$/", $product['specs'])) {
                                            echo "Weight: " . $product['specs'];
                                        } else if(preg_match("/^(\d+(?:,\d+)?)\s*(kb|KB|mb|MB|gb|GB)$/", $product['specs'])) {
                                            echo "Size: " . $product['specs'];
                                        } else {
                                            echo $product['specs'];
                                        }
                                    ?>
                                </dd>
                            </dl>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>
        </form>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>